{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
module Components where

import Protolude hiding (to)
import Language.Haskell.TH (mkName, Name)
import qualified Data.List.NonEmpty as NE
import Control.Lens (each, ix, Traversal', (^.), at, (%~), iso, Iso')
import Data.Generics.Labels()
import Data.Generics.Sum.Constructors (AsConstructor', _Ctor')
import Data.Generics.Product.Fields (HasField', field')

newtype Components t = Components (Map Name (NonEmpty t))
    deriving (Functor, Foldable, Traversable, Generic, Show)

asList :: Iso' (Maybe (NonEmpty a)) [a]
asList = iso (maybe [] NE.toList) NE.nonEmpty

addComponent :: Name -> t -> Components t -> Components t
addComponent name comp' = #_Components . at name . asList %~ (comp' :)

getComponent :: Name -> Components t -> [t]
getComponent name (Components m) = m ^. at name . asList

noComponents :: Components t
noComponents = Components mempty

components :: Name -> Traversal' (Components t) (NonEmpty t)
components name = #_Components . ix name

component :: Name -> Traversal' (Components t) t
component name = #_Components . ix name . each

comp :: forall name outer inner compType.
    ( HasField' "components" outer (Components compType)
    , KnownSymbol name, AsConstructor' name compType inner ) => Traversal' outer inner
comp = field' @"components" . component (mkName (symbolVal (Proxy @name))) . _Ctor' @name