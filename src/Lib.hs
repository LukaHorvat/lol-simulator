{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskellQuotes #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}
module Lib where

import Protolude hiding (cast, to, state)
import qualified Data.Map as Map
import Data.Generics.Labels()
import Ref
import Components
import Control.Lens ((<>~), (^?), (^.), (-~), (%~), (.~), view)
import qualified Data.List.NonEmpty as NE
import Data.List ((!!))
import Control.Lens (Ixed(ix))
import Prelude (Show(show))

data Unit = Unit
    { components :: Components UnitComponent
    , items :: [Ref Item]
    , spells :: [Ref Spell]
    , name :: Text }
    deriving (Show, Generic)

data Item = Item
    deriving (Show)

data Event = Event
    { time :: Double
    , eventType :: EventType }
    deriving (Show, Generic)

data EventType
    = Action (Ref Action)
    | OffCooldown Cooldown
    deriving (Show)

class Summarize a where
    summarize :: a -> Text
instance Summarize a => Summarize (Ref a) where
    summarize ref = summarize (getValue ref)
instance Summarize Action where
    summarize (Spellcast sp ctx) =
        ctx ^. #source . refValue . #name
        <> " casts "
        <> sp ^. refValue . #name
        <> " at "
        <> ctx ^. #target . refValue . #name
        <> case ctx ^. #proxy of
            Nothing -> ""
            Just prox -> " via " <> prox ^. refValue . #name
instance Summarize EventType where
    summarize (OffCooldown (SpellCooldown sp)) = getValue sp ^. #name <> " comes off cooldown"
    summarize (Action act) = summarize (getValue act)
instance Summarize a => Summarize [a] where
    summarize xs = foldMap ((<> "\n") . summarize) xs
instance Summarize a => Summarize (NonEmpty a) where
    summarize xs = foldMap ((<> "\n") . summarize) xs

instance Summarize Event where
    summarize Event{..} = "At " <> Protolude.show time <> " seconds " <> summarize eventType

data Spell = Spell
    { components :: Components SpellComponent
    , name :: Text }
    deriving (Show, Generic)

data Action
    = Spellcast (Ref Spell) SpellContext
    deriving (Show)

data Cooldown
    = SpellCooldown (Ref Spell)
    deriving (Show, Eq, Ord, Generic)

data UnitState = UnitState
    { components :: Components UnitComponent }
    deriving (Show, Generic)

data SpellContext = SpellContext
    { source :: Ref Unit
    , proxy :: Maybe (Ref Unit)
    , target :: Ref Unit }
    deriving (Show, Generic)

newtype SpellEffect = SpellEffect { getSpellEffect :: SpellContext -> SimState -> IO SimState }
instance Semigroup SpellEffect where
    SpellEffect f1 <> SpellEffect f2 = SpellEffect $ \tgt state -> f1 tgt state >>= f2 tgt
instance Monoid SpellEffect where
    mempty = SpellEffect (\_ x -> pure x)
instance Show SpellEffect where
    show _ = "SpellEffect"
data SpellComponent
    = StandardCooldown Double
    | Effect SpellEffect
    deriving (Show)

data UnitComponent
    = Health Double
    | Armor Double
    | MagicResistance Double
    | AbilityPower Double
    | AttackDamage Double
    | MagicAutoattack Double
    deriving (Show, Generic)

dummy :: UnitState -> IO (Ref Unit)
dummy UnitState{..} = newRef Unit{ items = [], spells = [], name = "Dummy", .. }

data SimState = SimState
    { unitStates :: Map (Ref Unit) UnitState
    , time :: Double
    , cooldowns :: Map Cooldown Double
    , pendingActions :: [NonEmpty (Ref Action)] }
    deriving (Show, Generic)

newtype EndoM m a = EndoM { getEndoM :: a -> m a }
instance Monad m => Semigroup (EndoM m a) where
    EndoM f1 <> EndoM f2 = EndoM (f1 >=> f2)
instance Monad m => Monoid (EndoM m a) where
    mempty = EndoM pure

initialUnitState :: Unit -> UnitState
initialUnitState Unit{..} = UnitState{..}

actionPossible :: SimState -> Action -> Bool
actionPossible SimState{..} = \case
    Spellcast spell _ -> Map.notMember (SpellCooldown spell) cooldowns

newtype SoonestEvent = SoonestEvent { getSoonestEvent :: Maybe Event }
instance Semigroup SoonestEvent where
    SoonestEvent (Just evt1) <> SoonestEvent (Just evt2)
        | view #time evt1 <= view #time evt2 = SoonestEvent (Just evt1)
        | otherwise = SoonestEvent (Just evt2)
    SoonestEvent (Just evt2) <> _ = SoonestEvent (Just evt2)
    _ <> evt2 = evt2
instance Monoid SoonestEvent where
    mempty = SoonestEvent Nothing

nextEvent :: SimState -> Maybe Event
nextEvent st@SimState{..} =
    getSoonestEvent (foldMap SoonestEvent [nextCooldown, availableAction])
    where
    availableAction = pendingActions
        & fmap NE.head
        & find (actionPossible st . getValue)
        <&> (\a -> Event time (Action a))
    nextCooldown = cooldowns
        & Map.toList
        & sortOn snd
        & headMay
        <&> (\(cd, cdTime) -> Event cdTime (OffCooldown cd))

applySpellComponent :: Ref Spell -> SpellContext -> SpellComponent -> SimState -> IO SimState
applySpellComponent spell ctx comp state = case comp of
    StandardCooldown cd -> state
        & #cooldowns %~ Map.insert
            (SpellCooldown spell)
            (state ^. #time + cd)
        & pure
    Effect (SpellEffect eff) -> eff ctx state

applyEvent :: SimState -> Event -> IO SimState
applyEvent st@SimState{..} (Event evtTime evt) = newState & case evt of
    OffCooldown cd -> pure
        . (#cooldowns %~ Map.filterWithKey (\k _ -> k /= cd))
    Action act ->
        (pure . (#pendingActions %~ mapMaybe
            (\acts -> if NE.head acts == act
                then NE.nonEmpty (NE.tail acts)
                else Just acts)))
        >=> case getValue act of
            Spellcast spell tgt -> spell
                & getValue
                & view #components
                & foldMap (EndoM . applySpellComponent spell tgt)
                & getEndoM
    where
    newState = st & #time .~ evtTime

applyFlatDamage :: Double -> SpellEffect
applyFlatDamage dmg = SpellEffect $ \ctx ->
    pure . (#unitStates . ix (ctx ^. #target) . comp @"Health" -~ dmg)

resistanceReduction :: Double -> Double
resistanceReduction resist = 100 / (100 + resist)

applyMagicDamage :: Double -> Double -> SpellEffect
applyMagicDamage apRatio dmg = SpellEffect $ \ctx simState -> simState
    & #unitStates . ix (ctx ^. #target) %~ (\unitState ->
        let sourceAp = fromMaybe 0 (simState ^? #unitStates . ix (ctx ^. #source) . comp @"AbilityPower")
            targetResist = fromMaybe 0 (unitState ^? comp @"MagicResistance")
        in unitState
            & comp @"Health" -~ (dmg + apRatio * sourceAp) * resistanceReduction targetResist)
    & pure

seedW :: IO (Ref Unit)
seedW = newRef Unit
    { components = noComponents
    , items = []
    , spells = []
    , name = "Seed" }

rampantGrowth :: Spell
rampantGrowth = Spell
    { components = noComponents
        & addComponent 'Effect (Effect eff)
    , name = "Rampant Growth" }
    where
    eff = SpellEffect $ \_ state -> do
        seed <- seedW
        state
            & #unitStates %~ Map.insert seed UnitState { components = noComponents }
            & pure

thornSpitterAuto :: Double -> IO (Ref Spell)
thornSpitterAuto autoDmg = newRef Spell
    { components = noComponents
        & addComponent 'StandardCooldown (StandardCooldown (1 / 0.8))
        & addComponent 'Effect (Effect (applyMagicDamage 0 autoDmg))
    , name = "Thorn Spitter Auto" }

thornSpitter :: Int -> Double -> IO (Ref Unit)
thornSpitter lvl zyraAp = do
    auto <- thornSpitterAuto (15.294 + 4.706 * fromIntegral lvl + 0.15 * zyraAp)
    newRef Unit
        { components = noComponents
        , name = "Thorn Spitter"
        , items = []
        , spells = [auto] }

getSpellByName :: Text -> Ref Unit -> Ref Spell
getSpellByName name unit = unit
    & getValue
    & spells
    & find ((== name) . view #name . getValue)
    & fromMaybe (panic ("Spell named " <> name <> " does not exist on " <> getValue unit ^. #name))

deadlySpines :: Int -> Spell
deadlySpines lvl = Spell
    { components = noComponents
        & addComponent 'StandardCooldown (StandardCooldown ([7, 6.5, 6, 5.5, 5] !! (lvl - 1)))
        & addComponent 'Effect (Effect eff)
    , name = "Deadly Spines" }
    where
    eff =
        applyMagicDamage 0.6 ([60, 95, 130, 165, 200] !! (lvl - 1))
        <> SpellEffect (\ctx state -> do
            let seedNum = state
                    & view #unitStates
                    & Map.keys
                    & toList
                    & filter ((== "Seed") . view #name . getValue)
                    & length
            let zyraAp = fromMaybe 0 $ state
                    ^? #unitStates . ix (ctx ^. #source) . comp @"AbilityPower"
            spitters <- replicateM seedNum (thornSpitter lvl zyraAp)
            spitterAttacks <- forM spitters $ \spit ->
                NE.fromList <$> mapM newRef (replicate 4 (spitterAutoAction ctx spit))
            let spittersStates = fmap (\s -> (s, initialUnitState (getValue s))) spitters
            state
                & #unitStates %~
                    ( Map.filterWithKey (\u _ -> getValue u ^. #name == "Seed")
                    . Map.union (Map.fromList spittersStates) )
                & #pendingActions <>~ spitterAttacks
                & pure
            )
    getSpitterAuto = getSpellByName "Thorn Spitter Auto"
    spitterAutoAction SpellContext{..} spit = Spellcast (getSpitterAuto spit) spellCtx
        where
        spellCtx = SpellContext { proxy = Just spit, .. }

zyra :: IO (Ref Unit)
zyra = do
    deadlySpines <- newRef (deadlySpines 5)
    rampantGrowth <- newRef rampantGrowth
    newRef Unit
        { components = noComponents
            & addComponent 'Health (Health 1000)
            & addComponent 'Armor (Armor 50)
            & addComponent 'MagicResistance (MagicResistance 50)
            & addComponent 'AbilityPower (AbilityPower 200)
        , items = []
        , spells = [deadlySpines, rampantGrowth]
        , name = "Zyra" }

simulate :: SimState -> IO [(Event, SimState)]
simulate state = case nextEvent state of
    Nothing -> return []
    Just evt -> do
        newState <- applyEvent state evt
        ((evt, newState) :) <$> simulate newState

sampleSituation :: IO SimState
sampleSituation = do
    zyra <- zyra
    dummy <- dummy UnitState { components = noComponents
        & addComponent 'Health (Health 1000)
        & addComponent 'Armor (Armor 50)
        & addComponent 'MagicResistance (MagicResistance 50) }
    rampGrowths <- mapM newRef $ replicate 2 $ Spellcast
        (getSpellByName "Rampant Growth" zyra)
        SpellContext
            { source = zyra, proxy = Nothing, target = dummy }
    deadlySpines <- newRef $ Spellcast
        (getSpellByName "Deadly Spines" zyra)
        SpellContext
            { source = zyra, proxy = Nothing, target = dummy }
    return SimState
        { unitStates = Map.fromList
            [ (zyra, initialUnitState (getValue zyra))
            , (dummy, initialUnitState (getValue dummy)) ]
        , time = 0
        , cooldowns = Map.empty
        , pendingActions = [NE.fromList (rampGrowths <> [deadlySpines])] }

test :: IO ()
test = do
    sit <- sampleSituation
    evts <- fmap fst <$> simulate sit
    putText (summarize evts)
