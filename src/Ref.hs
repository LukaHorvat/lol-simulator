module Ref where

import Protolude hiding (to)
import Data.Unique (newUnique, Unique)
import Prelude (Show(show))
import Control.Lens.Type (Getter)
import Control.Lens (to)

data Ref a = Ref
    { getValue :: a
    , getUnique :: Unique }
instance Eq (Ref a) where
    (==) = (==) `on` getUnique
instance Ord (Ref a) where
    compare = compare `on` getUnique
instance Show a => Show (Ref a) where
    show = Prelude.show . getValue

{-# NOINLINE newRef #-}
newRef :: a -> IO (Ref a)
newRef a = Ref a <$> newUnique

refValue :: Getter (Ref a) a
refValue = to getValue